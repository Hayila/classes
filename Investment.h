//#include <iostream>
//using namespace std;

#ifndef INVESTMENT_H
#define INVESTMENT_H

class Investment // 'C' is not supposed to be capitalized 
 {
    private:  //properties
         long double value;
         float interest_rate; 
    
    public:
         Investment(); //default constructor .... Name the "construtor"
          void setValue(long double val) ; // the 'val' in setval should be capitalised and its data type should be void 
          long double getValue();
		  void setInterest_rate(float int_rate);
		  float getInterest_rate();
		 
		 
 }; //semicolon ends class
 #endif

